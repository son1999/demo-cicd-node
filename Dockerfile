FROM node:14
RUN mkdir -p /usr/backend
WORKDIR /usr/backend
COPY package*.json ./
RUN npm install -g pm2
RUN yarn install
COPY . .
RUN yarn run build
EXPOSE 8085 80

ENV MONGO_URL "mongodb+srv://huynhtran:huynhtran@halongpay.dio9ns5.mongodb.net/?retryWrites=true&w=majority"
ENV IS_SECURED true
ENV TOKEN_EXPIRED_TIME -1
ENV AUTHORIZATION_PREFIX true
ENV HASKEY "halongpay2022"
ENV APP_KEY "halongpay2022@halongpay"
ENV BASE_URL_FMARKET_HASH "http://hmac-env.eba-es9qbywv.eu-central-1.elasticbeanstalk.com/"
ENV BASE_URL_FMARKET "https://rapi.fma.dev.techland.link/plus-partner/"

CMD [ "pm2", "start", "ecosystem.config.js", "--env", "development", "--no-daemon" ]