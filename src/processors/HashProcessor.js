import { axiosBase } from "../util/Axios";
import { validateRequestPushToFunction } from "../util/CommonUtil";

class HashProcessor {
  async getHash(data) {
    validateRequestPushToFunction(data);
    const isHash = true;
    const hashData = await axiosBase.post(process.env.BASE_URL_FMARKET_HASH, data, isHash);
    if (hashData) {
      return await hashData;
    } else {
      return false;
    }
  }

}

export const hashProcessor = new HashProcessor();
