export const HTTP_CODE = {
    OK: 200,
    INTERNAL_SERVER_ERROR: 500,
    FORBIDDEN: 403,
    BAD_REQUEST: 400
};

// URL api for core Fmarket
export const API_URL_FMARKET = {
    // account
    ACCOUNT_INFO: "accounts/get-account-info",
    ACCOUNT_TRADING_CONTRACT_LIST: "/accounts/trading-contract-list",
    ACCOUNT_REGISTER: "/accounts/register-account",
    ACCOUNT_IS_REGISTERED: "/accounts/is-register",

    // product
    PRODUCT_FILTER: "/markets/filter-products",
    PRODUCT_DETAIL: "/markets/get-product-detail",
    
    // static data
    STATIC_PROVINCES: "/static/provinces",
    STATIC_NATIONALITY: "/static/nationalities",
    STATIC_PLACE_OF_ISSUE_ID: "/static/place-of-issue-id-list",
    STATIC_BANKS: "/static/banks",
    STATIC_ALL_RESOURCES: "/static/all",

    // Trading - orders
    ORDER_ESTIMATE_PURCHASE_ORDER: "/orders/estimate-purchase",
    ORDER_CREATE_PURCHASE: "/orders/create-purchase-order/request",
    ORDER_CONFIRM_PURCHASE_ORDER: "orders/create-purchase-order/confirm",
    ORDER_ESTIMATE_SALE: "/orders/estimate-sale",
    ORDER_TRASACTION_HISTORY: "/orders/filter-transactions",
    ORDER_CREATE_SALE_ORDER: "/orders/create-sale-order/request",
    ORDER_CONFIRM_SALE_ORDER: "/orders/create-sale-order/confirm",

    // Assets 
    ASSET_TOTAL_MONEY: "/assets/filter-asset-funds",
    ASSET_DETAIL: "/assets/filter-assets",
}

// URL api for invest app HLP
export const API_URL_INVEST_APP = {
    // account
    ACCOUNT_HASH: "/account/hash",
    ACCOUNT_INFO: "/account/info",
    ACCOUNT_REGISTER: "/account/register-account",
    ACCOUNT_TRADING_CONTRACT_LIST: "/account/trading-contract-list",
    ACCOUNT_IS_REGISTERED: "/account/is-register",

    // product
    PRODUCT_FILTER: "/market/filter-products",
    PRODUCT_DETAIL: "/market/get-product-detail",

    // static data
    STATIC_PROVINCES: "/static/provinces",
    STATIC_NATIONALITY: "/static/nationalities",
    STATIC_PLACE_OF_ISSUE_ID: "/static/place-of-issue-id-list",
    STATIC_BANKS: "/static/banks",
    STATIC_ALL_RESOURCES: "/static/all",

    // Trading - orders
    ORDER_ESTIMATE_PURCHASE_ORDER: "/order/estimate-purchase",
    ORDER_CREATE_PURCHASE: "/order/create-purchase-order/request",
    ORDER_CONFIRM_PURCHASE_ORDER: "/order/create-purchase-order/confirm",
    ORDER_ESTIMATE_SALE: "/order/estimate-sale",
    ORDER_TRASACTION_HISTORY: "/order/filter-transactions",
    ORDER_CREATE_SALE_ORDER: "/order/create-sale-order/request",
    ORDER_CONFIRM_SALE_ORDER: "/order/create-sale-order/confirm",

    // Assets 
    ASSET_TOTAL_MONEY: "/asset/filter-asset-funds",
    ASSET_DETAIL: "/asset/filter-assets",
}