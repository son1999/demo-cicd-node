import cors from "cors";
import { urlencoded, json } from "body-parser";
import dotenv from "dotenv";
import http from "http";
import express from "express";
import { dbUtil } from "./util/DbUtil";
import RouteFactory from "./routes/RouteFactory";

dotenv.load();
var app = require("express")();
const api = express.Router();
app.use(urlencoded({ extended: true, limit: "500mb" }));
app.use(json({ extended: true, limit: "500mb" }));
app.use(cors());
dbUtil.connect();

app.get("/", (_, res) => {
  res.send("Running");
});

app.use("/api", api);
RouteFactory(api);
// our server instance
const server = http.createServer(app);
server.listen(8085, () =>
  console.log(`server is running on port 8085`)
);
