import { API_URL_INVEST_APP } from "../configs/constants";
import { orderController } from "../controllers/OrderController";

const OrderRoute = (app) => {
  app.route(API_URL_INVEST_APP.ORDER_ESTIMATE_PURCHASE_ORDER).post(orderController.estimatePurchaseOrder);
  app.route(API_URL_INVEST_APP.ORDER_CREATE_PURCHASE).post(orderController.createPurchaseOrder);
  app.route(API_URL_INVEST_APP.ORDER_CONFIRM_PURCHASE_ORDER).post(orderController.confirmPurchaseOrder);
  app.route(API_URL_INVEST_APP.ORDER_TRASACTION_HISTORY).post(orderController.getProductPurchasedByAccount);
  app.route(API_URL_INVEST_APP.ORDER_ESTIMATE_SALE).post(orderController.estimateSaleOrder);
  app.route(API_URL_INVEST_APP.ORDER_CREATE_SALE_ORDER).post(orderController.createSaleOrder);
  app.route(API_URL_INVEST_APP.ORDER_CONFIRM_SALE_ORDER).post(orderController.confirmSaleOrder);
};
export default OrderRoute;
