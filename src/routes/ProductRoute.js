import { API_URL_INVEST_APP } from "../configs/constants";
import { productController } from "../controllers/ProductController";

const ProductRoute = (app) => {
  app.route(API_URL_INVEST_APP.PRODUCT_DETAIL).post(productController.productDetail);
  app.route(API_URL_INVEST_APP.PRODUCT_FILTER).post(productController.productFilter);
};
export default ProductRoute;
