import { assetController } from "../controllers/AssetController";
import { API_URL_INVEST_APP } from "../configs/constants";

const AssetRoute = (app) => {
  app.route(API_URL_INVEST_APP.ASSET_TOTAL_MONEY).post(assetController.getTotalAsset);
  app.route(API_URL_INVEST_APP.ASSET_DETAIL).post(assetController.getAssetsDetail);
};
export default AssetRoute;
