import AuthRoute from "./AuthRoute";
import UserRoute from "./UserRoute";
import AccountRoute from "./AccountRoute";
import ProductRoute from "./ProductRoute";
import StaticDataRoute from "./StaticRoute";
import OrderRoute from "./OrderRoute";
import AssetRoute from "./AssetRoute";

export const RouteFactory = (app) => {
  AuthRoute(app);
  UserRoute(app);
  AccountRoute(app);
  ProductRoute(app);
  StaticDataRoute(app);
  OrderRoute(app);
  AssetRoute(app);
};

export default RouteFactory;