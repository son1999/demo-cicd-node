import { API_URL_INVEST_APP } from "../configs/constants";
import { staticDataController } from "../controllers/StaticDataController";

const StaticDataRoute = (app) => {
    app.route(API_URL_INVEST_APP.STATIC_PROVINCES).post(staticDataController.getProvince);
    app.route(API_URL_INVEST_APP.STATIC_NATIONALITY).post(staticDataController.getNationality);
    app.route(API_URL_INVEST_APP.STATIC_PLACE_OF_ISSUE_ID).post(staticDataController.getPlaceOfIssueId);
    app.route(API_URL_INVEST_APP.STATIC_BANKS).post(staticDataController.getBanks);
    app.route(API_URL_INVEST_APP.STATIC_ALL_RESOURCES).post(staticDataController.getAllResources);
};
export default StaticDataRoute;
