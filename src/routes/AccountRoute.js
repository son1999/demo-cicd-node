import { accountController } from "../controllers/AccountController";
import { API_URL_INVEST_APP } from "../configs/constants";

const AccountRoute = (app) => {
  app.route(API_URL_INVEST_APP.ACCOUNT_INFO).post(accountController.getAccountInfo);
  app.route(API_URL_INVEST_APP.ACCOUNT_TRADING_CONTRACT_LIST).post(accountController.getTradingContractList);
  app.route(API_URL_INVEST_APP.ACCOUNT_REGISTER).post(accountController.registerNewAccount);
  app.route(API_URL_INVEST_APP.ACCOUNT_IS_REGISTERED).post(accountController.checkAccount);
};
export default AccountRoute;
