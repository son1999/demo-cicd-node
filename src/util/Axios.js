import axios from "axios";

class AxiosBase {

    // method get using axios
    async get(url, params) {
        try {
            await axios.get(process.env.BASE_URL_FMARKET + url + `${params}`).then(results => {
                return results.data;
            })
                .catch(error => {
                    throw new Error(error);
                });
        } catch (error) {
            throw new Error(error);
        }
    }

    // method post using axios
    post = (url, data, isHash) => {
        try {
            if (isHash) {
                return new Promise((resolve, reject) => {
                    axios.post(process.env.BASE_URL_FMARKET_HASH, data).then(results => {
                        resolve(results.data);
                    }).catch(error => {
                        reject(error);
                    });
                })        
            } else {
                return new Promise((resolve, reject) => {
                    axios.post(process.env.BASE_URL_FMARKET + url, data).then(results => {
                        resolve(results.data);
                    })
                    .catch(error => {
                        reject(error);
                    });
                })
                
            }
        } catch (error) {
            throw new Error(error);
        }
    }
}

export const axiosBase = new AxiosBase();