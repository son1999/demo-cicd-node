import { API_URL_FMARKET, HTTP_CODE, INIT_MESSAGE } from "../configs/constants";
import { hashProcessor } from "../processors/HashProcessor";
import { axiosBase } from "../util/Axios";
import { validateRequestPushToFunction } from "../util/CommonUtil";
import { statusCode } from "../util/StatusCode";

class AssetController {
  // Get total money available of account
  async getTotalAsset(req, res) {
    validateRequestPushToFunction(req);

    const data = {
      fmarket_id: req.body?.fmarket_id,
      page: req.body?.page ?? 1,
      page_size: req.body?.page_size ?? 100
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }
    const totalAssets = await axiosBase.post(API_URL_FMARKET.ASSET_TOTAL_MONEY, dto);

    if (totalAssets) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: totalAssets,
        message: 'Get total money available of account success'
      })
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Get total money available of account fail'
      })
    }
  }

  // Assets detail
  async getAssetsDetail(req, res) {
    validateRequestPushToFunction(req);

    const data = {
      fmarket_id: req.body?.fmarket_id,
      page: req.body?.page ?? 1,
      page_size: req.body?.page_size ?? 100
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body?.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }
    
    const assetsDetail = await axiosBase.post(API_URL_FMARKET.ASSET_DETAIL, dto);

    if (assetsDetail) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: assetsDetail,
        message: 'Get assets detail success'
      })
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Get assets detail fail'
      })
    }
  }
}

export const assetController = new AssetController();
