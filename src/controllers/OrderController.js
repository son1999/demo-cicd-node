import { API_URL_FMARKET, HTTP_CODE } from "../configs/constants";
import { hashProcessor } from "../processors/HashProcessor";
import { axiosBase } from "../util/Axios";
import { validateRequestPushToFunction } from "../util/CommonUtil";
import { statusCode } from "../util/StatusCode";

class OrderController {
  // Calculate estimation of volume/fee for purchase order
  async estimatePurchaseOrder(req, res) {
    validateRequestPushToFunction(req);
    const data = {
      fmarket_id: req.body.fmarket_id,
      product_id: req.body.product_id,
      amount: req.body.amount
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const estimatePurchaseOrderData = await axiosBase.post(API_URL_FMARKET.ORDER_ESTIMATE_PURCHASE_ORDER, dto);
    
    if (estimatePurchaseOrderData) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: estimatePurchaseOrderData,
        message: 'Calculate estimation of volume/fee for purchase order success'
      })
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Calculate estimation of volume/fee for purchase order fail'
      })
    }
  }

  // Create purchase order
  async createPurchaseOrder(req, res) {
    validateRequestPushToFunction(req);

    const data = {
      fmarket_id: req.body.fmarket_id,
      product_id: req.body.product_id,
      amount: req.body.amount
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const createPurchaseOrderData = await axiosBase.post(API_URL_FMARKET.ORDER_CREATE_PURCHASE, dto);

    if (createPurchaseOrderData) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: createPurchaseOrderData,
        message: 'Create purchase order success'
      })
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Create purchase order fail'
      })
    }
  }

  // Confirm purchase order
  async confirmPurchaseOrder(req, res) {
    validateRequestPushToFunction(req);
    
    const data = {
      fmarket_id: req.body.fmarket_id,
      verification_id: req.body.verification_id
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const confirmPurchaseOrderData = await axiosBase.post(API_URL_FMARKET.ORDER_CONFIRM_PURCHASE_ORDER, dto);

    if (confirmPurchaseOrderData) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: confirmPurchaseOrderData,
        message: 'Confirm purchase order success'
      })
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Confirm purchase order fail'
      })
    }
  }

  // Estimate sale order
  async estimateSaleOrder(req, res) {
    validateRequestPushToFunction(req);
    
    const data = {
      fmarket_id: req.body.fmarket_id,
      product_id: req.body.product_id,
      volume: req.body.volume
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const estimateSaleOrderData = await axiosBase.post(API_URL_FMARKET.ORDER_ESTIMATE_SALE, dto);

    if (estimateSaleOrderData) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: estimateSaleOrderData,
        message: 'Calculate estimation of amount/fee for sale order success'
      })
    } else {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        message: 'Calculate estimation of amount/fee for sale order fail'
      })
    }
  }

  // Get product purchased by account
  async getProductPurchasedByAccount(req, res) {
    validateRequestPushToFunction(req);

    const products = [];

    const data = {
      fmarket_id: req.body.fmarket_id,
      page: req.body?.page ?? 1,
      page_size: req.body?.page_size ?? 100
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body?.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const productDataPurchasedByAccount = await axiosBase.post(API_URL_FMARKET.ORDER_TRASACTION_HISTORY, dto);
    if (productDataPurchasedByAccount) {
      productDataPurchasedByAccount.data.items.map((item) => {
        products.push(item);
      })

      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: products,
        message: 'Get list product purchased by account success'
      }) 
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Get list product purchased by account fail'
      })
    }
  }

  // Estimate sale order
  async estimateSaleOrder(req, res) {
    validateRequestPushToFunction(req);

    const data = {
      fmarket_id: req.body?.fmarket_id,
      product_id: req.body?.product_id,
      volume: req.body?.volume
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body?.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const estimateSaleOrder = await axiosBase.post(API_URL_FMARKET.ORDER_ESTIMATE_SALE, dto);

    if (estimateSaleOrder) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: estimateSaleOrder,
        message: 'Calculate estimation of amount/fee for sale order success'
      }) 
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Calculate estimation of amount/fee for sale order fail'
      }) 
    }
  }

  // Create sale order
  async createSaleOrder(req, res) {
    validateRequestPushToFunction(req);

    const data = {
      fmarket_id: req.body?.fmarket_id,
      product_id: req.body?.product_id,
      volume: req.body?.volume
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body?.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const newSaleOrder = await axiosBase.post(API_URL_FMARKET.ORDER_CREATE_SALE_ORDER, dto);
    
    if (newSaleOrder) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: newSaleOrder,
        message: 'Create sale order success'
      }) 
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Create sale order fail'
      }) 
    }
  }

  // Confirm sale order
  async confirmSaleOrder(req, res) {
    validateRequestPushToFunction(req);

    const data = {
      fmarket_id: req.body?.fmarket_id,
      verification_id: req.body?.verification_id,
      verification_token: req.body?.verification_token
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body?.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const confirmSaleOrderData = axiosBase.post(API_URL_FMARKET.ORDER_CONFIRM_SALE_ORDER, dto);

    if (confirmSaleOrderData) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: confirmSaleOrderData,
        message: 'Confirm sale order success'
      }) 
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Confirm sale order fail'
      }) 
    }
  }
}

export const orderController = new OrderController();
