import { API_URL_FMARKET, HTTP_CODE } from "../configs/constants";
import { hashProcessor } from "../processors/HashProcessor";
import { axiosBase } from "../util/Axios";
import { validateRequestPushToFunction } from "../util/CommonUtil";
import { statusCode } from "../util/StatusCode";

class StaticDataController {
    // Get province list
    async getProvince(req, res) {
        validateRequestPushToFunction(req);
        const data = {
            client_id: req.body.client_id,
            data: {
                page: req.body?.page ?? 1,
                page_size: req.body?.page_size ?? 100,
            }
        }

        const dataProvince = await axiosBase.post(API_URL_FMARKET.STATIC_PROVINCES, data);
        if (dataProvince) {
            return res.status(HTTP_CODE.OK).json({
                apiCode: statusCode.success,
                data: dataProvince,
                message: 'Get province success'
            })
        } else {
            return res.status(HTTP_CODE.BAD_REQUEST).json({
                apiCode: statusCode.generalError,
                message: 'Get province fail'
            })
        }
    }

    // Get nationality list available on fmarket.vn
    async getNationality(req, res) {
        validateRequestPushToFunction(req);
        const data = {
            client_id: req.body.client_id,
            data: {
                page: req.body?.page ?? 1,
                page_size: req.body?.page_size ?? 100,
            }
        }

        const dataNationality = await axiosBase.post(API_URL_FMARKET.STATIC_NATIONALITY, data);

        if (dataNationality) {
            return res.status(HTTP_CODE.OK).json({
                apiCode: statusCode.success,
                data: dataNationality,
                message: 'Get nationality list success'
            })
        } else {
            return res.status(HTTP_CODE.BAD_REQUEST).json({
                apiCode: statusCode.generalError,
                message: 'Get nationality list fail'
            })
        }
    }

    // Get place of issue id list 
    async getPlaceOfIssueId(req, res) {
        validateRequestPushToFunction(req);
        const data = {
            client_id: req.body.client_id,
            data: {
                page: req.body?.page ?? 1,
                page_size: req.body?.page_size ?? 100,
            }
        }

        const dataPlaceOfIssueId = await axiosBase.post(API_URL_FMARKET.STATIC_PLACE_OF_ISSUE_ID, data);
        
        if (dataPlaceOfIssueId) {
            return res.status(HTTP_CODE.OK).json({
                apiCode: statusCode.success,
                data: dataPlaceOfIssueId,
                message: 'Get place of issue id list success'
            })
        } else {
            return res.status(HTTP_CODE.BAD_REQUEST).json({
                apiCode: statusCode.generalError,
                message: 'Get place of issue id list fail'
            })
        }
    }

    // Get list bank
    async getBanks(req, res) {
        validateRequestPushToFunction(req);

        const data = {
            client_id: req.body.client_id,
            data: {
                page: req.body?.page ?? 1,
                page_size: req.body?.page_size ?? 100,
            }
        }

        const dataBanks = await axiosBase.post(API_URL_FMARKET.STATIC_BANKS, data);
        
        if (dataBanks) {
            return res.status(HTTP_CODE.OK).json({
                apiCode: statusCode.success,
                data: dataBanks,
                message: 'Get list bank success'
            })
        } else {
            return res.status(HTTP_CODE.BAD_REQUEST).json({
                apiCode: statusCode.generalError,
                message: 'Get list bank fail'
            })
        }
    }

    // Get all resources
    async getAllResources(req, res) {
        validateRequestPushToFunction(req);

        const data = {
            client_id: req.body.client_id,
            data: {
                
            }
        }

        const dataAllResources = await axiosBase.post(API_URL_FMARKET.STATIC_ALL_RESOURCES, data);
        if (dataAllResources) {
            return res.status(HTTP_CODE.OK).json({
                apiCode: statusCode.success,
                data: dataAllResources,
                message: 'Get all resources success'
            })
        } else {
            return res.status(HTTP_CODE.BAD_REQUEST).json({
                apiCode: statusCode.generalError,
                message: 'Get all resources fail'
            })
        }
    }
}

export const staticDataController = new StaticDataController();
