import { API_URL_FMARKET, HTTP_CODE, INIT_MESSAGE } from "../configs/constants";
import { hashProcessor } from "../processors/HashProcessor";
import { axiosBase } from "../util/Axios";
import { validateRequestPushToFunction } from "../util/CommonUtil";
import { statusCode } from "../util/StatusCode";

class AccountController {
  // Get account info
  async getAccountInfo(req, res) {
    validateRequestPushToFunction(req);
    const hashCode = await hashProcessor.getHash(req.body.fmarket_id);
    const data = {
      client_id: req.body.client_id,
      data: {
        fmarket_id: req.body.fmarket_id,
      },
      hash: hashCode.replace('Hash: ', '').trim()
    }
    const dataAccount = await axiosBase.post(API_URL_FMARKET.ACCOUNT_INFO, data);

    if (dataAccount) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: dataAccount,
        message: 'Get Account info success'
      })
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Get Account info fail'
      })
    }
  }

  // Get trading contract list
  async getTradingContractList(req, res) {
    validateRequestPushToFunction(req);
    const hashCode = await hashProcessor.getHash(req.body.fmarket_id);
    
    const data = {
      client_id: req.body.client_id,
      data: {
        fmarket_id: req.body.fmarket_id
      },
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const dataTradingContract = await axiosBase.post(API_URL_FMARKET.ACCOUNT_TRADING_CONTRACT_LIST, data);
    if (dataTradingContract) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: dataTradingContract,
        message: 'Get trading contract list success'
      })
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Get trading contract list fail'
      })
    }
  }

  // Register new account
  async registerNewAccount(req, res) {
    validateRequestPushToFunction(req);

    const data = {
      full_name: req.body?.full_name,
      email: req.body?.email,
      phone_postal: req.body?.phone_postal,
      cell_phone: req.body?.cell_phone,
      gender: req.body?.gender,
      dob: req.body?.dob,
      nationality_id: req.body?.nationality_id,
      province_id: req.body?.province_id,
      id_type: req.body?.id_type,
      id_no: req.body?.id_no,
      id_date_of_issue: req.body?.id_date_of_issue,
      id_place_of_issue: req.body?.id_place_of_issue,
      id_front_photo_base64: req.body?.id_front_photo_base64,
      id_back_photo_base64: req.body?.id_back_photo_base64,
      permanent_address: req.body?.permanent_address,
      mailing_address: req.body?.mailing_address,
      bank_account_no: req.body?.bank_account_no,
      bank_branch: req.body?.bank_branch,
      bank_id: req.body?.bank_id
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body?.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const newAccountData = await axiosBase.post(API_URL_FMARKET.ACCOUNT_REGISTER, dto);
    if (newAccountData) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: newAccountData,
        message: 'register new account success'
      })
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'register new account fail'
      })
    }
  }

  // Check if an account is already registered
  async checkAccount(req, res) {
    validateRequestPushToFunction(req);

    const data = {
      id_no: req.body.id_no,
      cell_phone: req.body.cell_phone,
      phone_postal: req.body.phone_postal
    }

    const hashCode = await hashProcessor.getHash(data);
    const dto = {
      client_id: req.body.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const isAccountRegistered = await axiosBase.post(API_URL_FMARKET.ACCOUNT_IS_REGISTERED, dto);
    
    if (isAccountRegistered) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: isAccountRegistered,
        message: 'Checking account is registered success'
      })
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Checking account is registered fail'
      })
    }
  }

}

export const accountController = new AccountController();
