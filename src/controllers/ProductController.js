import { API_URL_FMARKET, HTTP_CODE } from "../configs/constants";
import { hashProcessor } from "../processors/HashProcessor";
import { axiosBase } from "../util/Axios";
import { validateRequestPushToFunction } from "../util/CommonUtil";
import { statusCode } from "../util/StatusCode";

class ProductController {
  // Retrieve Fmarket’s product detail
  async productDetail(req, res) {
    validateRequestPushToFunction(req);
    const data = {
      product_id: req.body.product_id
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const productDetail = await axiosBase.post(API_URL_FMARKET.PRODUCT_DETAIL, dto);
    
    if (productDetail) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: productDetail,
        message: 'Get detail product success'
      })
    } else {
      return res.status(HTTP_CODE.BAD_REQUEST).json({
        apiCode: statusCode.generalError,
        message: 'Get detail product fail'
      })
    }
  }

  // Query list of products (Product Filter)
  async productFilter(req, res) {
    validateRequestPushToFunction(req);

    const data = {
      page: req.body.page ?? 1,
      page_size: req.body.page_size ?? 100,
      fundAssetTypes: req.body?.fundAssetTypes,
      sortField: req.body?.sortField,
      sortOrder: req.body?.sortOrder
    }

    const hashCode = await hashProcessor.getHash(data);

    const dto = {
      client_id: req.body.client_id,
      data: data,
      hash: hashCode.replace('Hash: ', '').trim()
    }

    const productFilter = await axiosBase.post(API_URL_FMARKET.PRODUCT_FILTER, dto);

    if (productFilter) {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.success,
        data: productFilter,
        message: 'Get products success'
      })
    } else {
      return res.status(HTTP_CODE.OK).json({
        apiCode: statusCode.generalError,
        message: 'Get products fail'
      })
    }
  }
}

export const productController = new ProductController();
